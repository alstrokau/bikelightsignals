#bikeLightSingals
##/info/
**date|**070616
**ver|**01
**author|**alstrokau  
**aim|**Turn and stop async ligths for bike light signals indication.

##/description/
This system is to perform light indication for bike - stop and turn right/left signals.
It's intended that system will have front and rear turn leds, rear stop leds and turn/stop lights on driver's panel.  

##/hardware/
So, hardware configuration is following:

- 4 buttons  
- 8 leds:  

  * 6 turns
  * 2 stops

###//buttons//
|code|description|
|:--:|:----------|
|IST| stop button|  
|IAL| alarm button|  
|ITR| turn right button|
|ITL| turn left button|  

###//leds//
|code|description|
|:--:|:----------|
|OSTR| stop rear led|  
|OSTP| stop panel led|  
|OTRR| turn right rear led|  
|OTRP| turn right panel led|
|OTRF| turn right front led|
|OTLR| turn left rear led|
|OTLP| turn left panel led
|OTLF| turn left front led

##/arduino pinout/
|#|code|pin|name|  
|-|----|---|---|  
|leds|
|1|OSTR|2|D2|
|2|OSTP|3|D3|
|3|OTRR|4|D4|
|4|OTRP|5|D5|
|5|OTRF|6|D6|
|6|OTLR|7|D7|
|7|OTLP|8|D8|
|8|OTLF|9|D9|
|buttons||||
|9 |IST|14|A0|
|10|IAL|15|A1|
|11|ITR|16|A2|
|12|ITL|17|A3|