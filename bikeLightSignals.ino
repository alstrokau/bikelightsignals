/* see bikeLightSignals.md for documentation */

unsigned long timeout = 200;
unsigned long prevtime = 0;
int state = HIGH;

const int PIN_OSTR = 2;
const int PIN_OSTP = 3;
const int PIN_OTRR = 4;
const int PIN_OTRP = 5;
const int PIN_OTRF = 6;
const int PIN_OTLR = 7;
const int PIN_OTLP = 8;
const int PIN_OTLF = 9;

const int PIN_IST = 14;
const int PIN_IAL = 15;
const int PIN_ITR = 16;
const int PIN_ITL = 17;

int input_turn_left;
int input_turn_right;
int input_stop;
int input_alarm;


///	startup code.
void setup() {
	setupLeds();
	setupButtons();
	prevtime = millis();
}

///	startup code for leds.
void setupLeds(){
	pinMode(PIN_OSTR, OUTPUT);	
	pinMode(PIN_OSTP, OUTPUT);
	pinMode(PIN_OTRR, OUTPUT);
	pinMode(PIN_OTRP, OUTPUT);
	pinMode(PIN_OTRF, OUTPUT);
	pinMode(PIN_OTLR, OUTPUT);
	pinMode(PIN_OTLP, OUTPUT);
	pinMode(PIN_OTLF, OUTPUT);
}

///	startup code for buttons.
void setupButtons(){
	pinMode(PIN_IST, INPUT_PULLUP);
	pinMode(PIN_IAL, INPUT_PULLUP);
	pinMode(PIN_ITR, INPUT_PULLUP);
	pinMode(PIN_ITL, INPUT_PULLUP);
}


///	primary code
void loop() {
	if( millis() - prevtime >= timeout ) {
		state = !state;
		prevtime = millis();
	}

	processTurn();
	processStop();
	processAlarm();
}

void processTurn(){
	input_turn_left = digitalRead(PIN_ITL);
	if(input_turn_left){
		digitalWrite(PIN_OTLF, LOW);
		digitalWrite(PIN_OTLR, LOW);
		digitalWrite(PIN_OTLP, LOW);
	}else{
		digitalWrite(PIN_OTLF, state);
		digitalWrite(PIN_OTLR, state);
		digitalWrite(PIN_OTLP, state);		
	}
	
	input_turn_right = digitalRead(PIN_ITR);
	if(input_turn_right){
		digitalWrite(PIN_OTRF, LOW);
		digitalWrite(PIN_OTRR, LOW);
		digitalWrite(PIN_OTRP, LOW);
	}else{                 
		digitalWrite(PIN_OTRF, state);
		digitalWrite(PIN_OTRR, state);
		digitalWrite(PIN_OTRP, state);		
	}	
}

void processStop(){
	input_stop = digitalRead(PIN_IST);
	digitalWrite( PIN_OSTP, !input_stop );
	digitalWrite( PIN_OSTR, !input_stop );
}

void processAlarm(){
	input_alarm = digitalRead(PIN_IAL);
	if(input_alarm){
		digitalWrite(PIN_OTLF, LOW);
		digitalWrite(PIN_OTLR, LOW);
		digitalWrite(PIN_OTLP, LOW);
		digitalWrite(PIN_OTRF, LOW);
		digitalWrite(PIN_OTRR, LOW);
		digitalWrite(PIN_OTRP, LOW);
	}else{
		digitalWrite(PIN_OTLF, state);
		digitalWrite(PIN_OTLR, state);
		digitalWrite(PIN_OTLP, state);
		digitalWrite(PIN_OTRF, state);
		digitalWrite(PIN_OTRR, state);
		digitalWrite(PIN_OTRP, state);		
	}
}

